<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Labor extends Model
{
	use SoftDeletes;
    protected $table = 'labor';
    protected $fillable = ['id','descripcion'];
    public $timestamps = false;
    protected $dates = ['deleted_at'];    
}
