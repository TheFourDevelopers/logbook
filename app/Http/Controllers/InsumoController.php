<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Insumo;
<<<<<<< HEAD
use App\Unidad;
=======
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
use App\Categoria_Insumo;
use Validator;
class InsumoController extends Controller
{

  public function __construct(){
<<<<<<< HEAD
    $this->middleware('auth');

  }
=======
$this->middleware('auth');

}
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $categorias =\DB::connection($connection)->table('insumo')
=======
      $categorias =\DB::table('insumo')
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      ->join('categoria_insumo', 'categoria_insumo.id', '=', 'insumo.id_categoria_insumo')
      ->distinct()
      ->select('categoria_insumo.*')
      ->get();
<<<<<<< HEAD
      $insumos = Insumo::on($connection)->get();
=======
      $insumos = Insumo::all();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      return view('insumos.index', compact('insumos','categorias'));
    }
    /**
     * Funcion que valida los campos y la bd
     */
    public function validator(Request $request)
    {
      $validator = Validator::make($request->all(), [
          'descripcion' => 'required|max:25|unique:insumo,descripcion',
<<<<<<< HEAD
          'id_unidad' => 'required',
=======
          'unidad_medida' => 'required',
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
          'precio_compra' => 'required',
          'id_categoria_insumo' => 'required'
      ]);
      return $validator;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $categorias = Categoria_Insumo::on($connection)->get();
      $unidades=Unidad::on($connection)->get();
      return view('insumos.create',compact('categorias','unidades'));
=======
      $categorias = Categoria_Insumo::all();
      return view('insumos.create',compact('categorias'));
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $insumo=Insumo::on($connection)->withTrashed()->where('descripcion', $request->descripcion)->first();
=======
      $insumo=Insumo::withTrashed()->where('descripcion', $request->descripcion)->first();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      if ($insumo!=null && $insumo->trashed())
      {
          $insumo->restore();
          $insumo->update($request->all());
          $Insumo->cantidad_disponible=0;
          return redirect('insumos');
      }
      $insumo = new Insumo();
      if ($this->validator($request)->fails())
      {
          return redirect('insumos/create')
          ->withErrors($this->validator($request))->withInput();
      }
<<<<<<< HEAD
      $insumo::on( $connection)->create($request->all());
=======
      $insumo->create($request->all());
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      return redirect('insumos');
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $insumo  = Insumo::on( $connection)->find($id);
      $categoria = Categoria_Insumo::on( $connection)->get();
      $unidades = Unidad::on( $connection)->get();
      return view('insumos.edit',compact('insumo','categoria','unidades'));
=======
      $insumo  = Insumo::find($id);
      $categoria = Categoria_Insumo::all();
      return view('insumos.edit',compact('insumo','categoria'));
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    }
     /**
     * Show the form for recharge the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function recharge($id)
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $insumo  = Insumo::on( $connection)->find($id);
=======
      $insumo  = Insumo::find($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      return view('insumos.recharge',compact('insumo'));
    }
    /**To save the recharged supply*/
    public function save_recharge(Request $request, $id)
<<<<<<< HEAD
    {   $connection =  session()->get('key');
        $insumo = Insumo::on( $connection)->find($id);
=======
    {
        $insumo = Insumo::find($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
        $insumo->cantidad_disponible  += $request->cant_recargar;
        $insumo->save();
        return redirect('insumos');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $insumo = Insumo::on( $connection)->findOrFail($id);
      $validator = Validator::make($request->all(), [
        'descripcion' => 'required|max:25|unique:insumo,descripcion,'.$id,
        'id_unidad' => 'required',
=======
      $insumo = Insumo::findOrFail($id);
      $validator = Validator::make($request->all(), [
        'descripcion' => 'required|max:25|unique:insumo,descripcion,'.$id,
        'unidad_medida' => 'required',
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
        'precio_compra' => 'required',
        'id_categoria_insumo' => 'required'
      ]);
      if ($validator->fails()) {
          return redirect()->back()->withErrors($validator)->withInput();
      }
      $insumo->update($request->all());
      return redirect('insumos');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $insumo = Insumo::on( $connection)->find($id);
=======
      $insumo = Insumo::find($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      $insumo->delete();
      return redirect('insumos');
    }
}
