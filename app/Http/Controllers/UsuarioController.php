<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
<<<<<<< HEAD
use Auth;
=======
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
class UsuarioController extends Controller
{

  public function __construct(){
<<<<<<< HEAD

            $this->middleware('auth');
            //$this->middleware('admin');
            $this->middleware('admin',['except'=>['edit','updatePassword','update']]);
=======
     $this->middleware('auth');
  $this->middleware('admin',['except'=>['edit','updatePassword']]);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
<<<<<<< HEAD

     */
    public function index()
    {
      $connection =  session()->get('key');
      $usuarios = User::on( $connection)->get();
=======
     */
    public function index()
    {

      $usuarios = User::all();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      return view('auth.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $usuarios = User::on( $connection)->get();
=======
      $usuarios = User::all();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      return view('auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
<<<<<<< HEAD

=======
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      $validator =  Validator::make($request->all(), [
        'name' => 'required|max:255',
        'email' => 'required|max:255|unique:users',
        'password' => 'required|confirmed|min:6',
        'lastname' => 'required',
        ]);
      if ($validator->fails()) {

           return redirect()->back()->withErrors($validator->errors());
      };
      User::create([
     'name' => $request->name,
     'email' => $request->email,
     'lastname' => $request->lastname,
     'username' => $request->username,
     'password' => bcrypt($request->password)
   ]);
<<<<<<< HEAD
=======

>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
   return redirect('usuarios');
    }


    /**
<<<<<<< HEAD
=======
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
<<<<<<< HEAD
       if (Auth::user()->id==$id || Auth::user()->isAdmin ==1) {

         $connection =  session()->get('key');
         $user = User::on( $connection)->find($id);
         if (is_null($user)) {
          return Redirect::back()->with('message', 'Usuario no encotrado');
         }
         return view('auth.edit',compact('user'));
       }
       return response()->view('errors.404');
=======
       $user  = User::find($id);
       return view('auth.edit',compact('user'));
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
     }


     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
<<<<<<< HEAD
       $connection =  session()->get('key');
       $user = User::on( $connection)->findOrFail($id);
       $user->update($request->all());
       if (Auth::user()->isAdmin ==1) {
         return redirect('usuarios');
       }
       return redirect('home'); 
      }
=======
       $user = User::findOrFail($id);
       $user->update($request->all());
       return redirect('usuarios');     }
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $user = User::on( $connection)->find($id);
=======
      $user = User::find($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      $user->delete();
      return redirect('usuarios');
    }
    public function updatePassword( Request $request)
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
       $user = User::on( $connection)->findOrFail($request->id);
        $user->fill([
            'password' => Hash::make($request->password)
        ])->save();
        return Redirect::back()->with('message', 'Contraseña Actualizada');

=======
        //return redirect('home');

       $user = User::findOrFail($request->id);

        $user->fill([
            'password' => Hash::make($request->password)
        ])->save();

        return Redirect::back()->with('message', 'Contraseña Actualizada');


>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    }


}
