<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Categoria_insumos;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Categoria_Insumo;
use Validator;
class Categoria_insumosController extends Controller
{

  public function __construct(){
     $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
<<<<<<< HEAD
        $connection =  session()->get('key');
        $categoria_insumos = Categoria_Insumo::on( $connection)->get();
=======
        $categoria_insumos = Categoria_Insumo::all();

>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
        return view('categoria_insumos.index', compact('categoria_insumos'));
    }
/**
 * Funcion que valida los campos
 */
    public function validator(Request $request)
    {
      $validator = Validator::make($request->all(), [
          'descripcion' => 'required|max:25|unique:categoria_insumo,descripcion'
      ]);
      return $validator;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('categoria_insumos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $categoria_insumos=Categoria_Insumo::on( $connection)->withTrashed()->where('descripcion', $request->descripcion)->first();
=======
      $categoria_insumos=Categoria_Insumo::withTrashed()->where('descripcion', $request->descripcion)->first();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c

      if ($categoria_insumos!=null && $categoria_insumos->trashed())
      {
          $categoria_insumos->restore();
          return redirect('categoria_insumos');
      }
      if ($this->validator($request)->fails())
      {
          return redirect('categoria_insumos/create')
          ->withErrors($this->validator($request))->withInput();
      }
<<<<<<< HEAD
        Categoria_Insumo::on( $connection)->create($request->all());
=======
        Categoria_Insumo::create($request->all());
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
        return redirect('categoria_insumos');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
<<<<<<< HEAD
       $connection =  session()->get('key');
        $categoria_insumo = Categoria_Insumo::on( $connection)->findOrFail($id);
=======
        $categoria_insumo = Categoria_Insumo::findOrFail($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c

        return view('categoria_insumos.edit', compact('categoria_insumo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
<<<<<<< HEAD
        $connection =  session()->get('key');
        $categoria_insumo = Categoria_Insumo::on( $connection)->findOrFail($id);
=======
        $categoria_insumo = Categoria_Insumo::findOrFail($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
        $validator = Validator::make($request->all(), [
            'descripcion' => 'required|max:25|unique:categoria_insumo,descripcion,'.$id,
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $categoria_insumo->update($request->all());
        return redirect('categoria_insumos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $categoria_Insumo = Categoria_Insumo::on( $connection)->find($id);
      $categoria_Insumo->delete();
      return redirect('categoria_insumos');
=======
        Categoria_Insumo::destroy($id);
        Session::flash('flash_message', 'Categoria_insumos successfully deleted!');
        return redirect('categoria_insumos');
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    }
}
