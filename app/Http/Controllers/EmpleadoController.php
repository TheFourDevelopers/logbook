<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Empleado;
use App\Categoria_Empleado;
use Validator;
class EmpleadoController extends Controller
{

  public function __construct(){
    $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $categorias =\DB::connection($connection)->table('empleado')
=======
      $categorias =\DB::table('empleado')
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      ->join('categoria_empleado', 'categoria_empleado.id', '=', 'empleado.id_categoria_empleado')
      ->distinct()
      ->select('categoria_empleado.*')
      ->get();
<<<<<<< HEAD
      $empleados = Empleado::on( $connection)->get();
=======
      $empleados = Empleado::all();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      return view('empleados.index', compact('empleados','categorias'));
    }

    /**
     * Funcion que valida los campos y la bd
     */
    public function validator(Request $request)
    {
      $validator = Validator::make($request->all(), [
          'cedula' => 'required|max:25|unique:empleado,cedula',
          'nombre' => 'required|max:25',
          'apellidos' => 'required|max:25',
          'domicilio' => 'required|max:25',
          'id_categoria_empleado' => 'required',
          'telefono_casa' => 'required|max:25',
          'telefono_personal' => 'required|max:25'
      ]);
      return $validator;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $categorias = Categoria_Empleado::on( $connection)->get();
=======
      $categorias = Categoria_Empleado::all();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      return view('empleados.create',compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $empleado=Empleado::on( $connection)->withTrashed()->where('cedula', $request->cedula)->first();
=======
      $empleado=Empleado::withTrashed()->where('cedula', $request->cedula)->first();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      if ($empleado!=null && $empleado->trashed())
      {
          $empleado->restore();
          $empleado->update($request->all());
          return redirect('empleados');
      }
      $empleado = new Empleado();
      if ($this->validator($request)->fails())
      {
          return redirect('empleados/create')
          ->withErrors($this->validator($request))->withInput();
      }
<<<<<<< HEAD
      $empleado::on( $connection)->create($request->all());
=======
      $empleado->create($request->all());
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      return redirect('empleados');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
     $empleado  = Empleado::on( $connection)->find($id);
     $categoria = Categoria_Empleado::on( $connection)->get();
=======
     $empleado  = Empleado::find($id);
     $categoria = Categoria_Empleado::all();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
     return view('empleados.edit',compact('empleado','categoria'));
   }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update($id, Request $request)
     {
<<<<<<< HEAD
       $connection =  session()->get('key');
       $empleado = Empleado::on( $connection)->findOrFail($id);
=======
       $empleado = Empleado::findOrFail($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
       $validator = Validator::make($request->all(), [
         'cedula' => 'required|max:25|unique:empleado,cedula,'.$id,
         'nombre' => 'required',
         'apellidos' => 'required',
         'domicilio' => 'required',
         'id_categoria_empleado' => 'required',
         'telefono_casa' => 'required',
         'telefono_personal' => 'required'
       ]);
       if ($validator->fails()) {
           return redirect()->back()->withErrors($validator)->withInput();
       }
       $empleado->update($request->all());
       return redirect('empleados');
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $empleado = Empleado::on( $connection)->find($id);
=======
      $empleado = Empleado::find($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      $empleado->delete();
      return redirect('empleados');
    }
  }
