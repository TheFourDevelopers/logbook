<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Categoria_Empleado;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Validator;

class Categoria_empleadoController extends Controller
{
<<<<<<< HEAD

  public function __construct(){
     $this->middleware('auth');

=======
  public function __construct(){
     $this->middleware('auth');
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
  }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $categoria_empleados = Categoria_Empleado::on( $connection)->orderBy('id', 'DESC')->get();
      return view('categoria_empleado.index', compact('categoria_empleados'));
=======
        $categoria_empleados = Categoria_Empleado::orderBy('id', 'DESC')->get();

        return view('categoria_empleado.index', compact('categoria_empleados'));
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    }

    public function validator(Request $request)
    {
      $validator = Validator::make($request->all(), [
          'descripcion' => 'required|max:25|unique:categoria_empleado,descripcion',
          'monto_x'     => 'required',
      ]);
      return $validator;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('categoria_empleado.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */

    public function store(Request $request)
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $categoria_empleado=Categoria_Empleado::on( $connection)->withTrashed()->where('descripcion', $request->descripcion)->first();
=======
      $categoria_empleado=Categoria_Empleado::withTrashed()->where('descripcion', $request->descripcion)->first();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c

      if ($categoria_empleado!=null && $categoria_empleado->trashed())
      {
          $categoria_empleado->restore();
          $categoria_empleado->update($request->all());
          return redirect('categoria_empleado');
      }
      if ($this->validator($request)->fails()) {
          return redirect('categoria_empleado/create')
          ->withErrors($this->validator($request))->withInput();
      }
<<<<<<< HEAD
        Categoria_Empleado::on( $connection)->create($request->all());
=======
        Categoria_Empleado::create($request->all());
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
        return redirect('categoria_empleado');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $categoria_empleado = Categoria_empleado::on( $connection)->findOrFail($id);
      return view('categoria_empleado.edit', compact('categoria_empleado'));
=======
        $categoria_empleado = Categoria_empleado::findOrFail($id);

        return view('categoria_empleado.edit', compact('categoria_empleado'));
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */

    public function update($id, Request $request)
    {
<<<<<<< HEAD
        $connection =  session()->get('key');
        $categoria_empleado = Categoria_empleado::on( $connection)->findOrFail($id);
=======
        $categoria_empleado = Categoria_empleado::findOrFail($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
        $validator = Validator::make($request->all(), [
            'descripcion' => 'required|max:25|unique:categoria_empleado,descripcion,'.$id,
            'monto_x'     => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $categoria_empleado->update($request->all());
        return redirect('categoria_empleado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
<<<<<<< HEAD
        $connection =  session()->get('key');
        $categoria_empleado = Categoria_empleado::on( $connection)->findOrFail($id);
        $categoria_empleado->delete();
=======
        Categoria_empleado::destroy($id);

>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
        Session::flash('flash_message', 'Categoria_empleado successfully deleted!');

        return redirect('categoria_empleado');
    }

}
