<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Empleado;
use App\Labor;
use App\Lote;
use App\Tarea;
use App\Producto;
use App\Lote_Tarea;
use App\Empleado_Tarea;
use App\Insumo;
use App\Categoria_Insumo;
use App\Categoria_Empleado;
use App\Tarea_Insumo;
use DB;
class ReporteController extends Controller
{

  public function __construct(){
    $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $Labores = Labor::all();
      $Productos = Producto::all();
      $tareas=Tarea::createdLastXDays(30)->get();
      $table_lote_tarea=Lote_Tarea::all();
      $Lotes=Lote::all();
      $Empleados=empleado::all();
      $table_empleado_tarea=empleado_tarea::all();
      $Insumos=Insumo::all();
      $table_tarea_insumo=tarea_insumo::all();
<<<<<<< HEAD
=======


>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      return view('reportes.index', compact('tareas','Labores','Productos','table_lote_tarea','Lotes','table_empleado_tarea','Insumos','table_tarea_insumo','Empleados'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
<<<<<<< HEAD
     return view('reportes.index');
=======



     return view('reportes.index');


>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
<<<<<<< HEAD
    {

    }
=======
    {}
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showLabores()
    {
      $fecha_inicial=$request->fecha_inicial;
      $fecha_final=$request->fecha_final;
<<<<<<< HEAD
      $tareas=DB::table('Tarea')
      ->whereBetween('fecha',array($fecha_inicial,$fecha_final))
      ->get();
=======

      $tareas=DB::table('Tarea')
      ->whereBetween('fecha',array($fecha_inicial,$fecha_final))
      ->get();


>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      $Labores = Labor::all();
      $Productos = Producto::all();
      $table_lote_tarea=Lote_Tarea::all();
      $Lotes=Lote::all();
      $Empleados=Empleado::all();
      $table_empleado_tarea=Empleado_Tarea::all();
      $Insumos=Insumo::all();
      $table_tarea_insumo=Tarea_Insumo::all();
<<<<<<< HEAD
      return view('reportes.labores', compact('tareas','Labores','Productos','table_lote_tarea','Lotes','table_empleado_tarea','Insumos','table_tarea_insumo','Empleados'));
=======


      return view('reportes.labores', compact('tareas','Labores','Productos','table_lote_tarea','Lotes','table_empleado_tarea','Insumos','table_tarea_insumo','Empleados'));

>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    }
    /*Para generar el reporte semanal de los empleados*/
    public function showEmpleados(Request $request)
    {
<<<<<<< HEAD
      $connection =  session()->get('key');
      $fecha_inicial=$request->fecha_inicial;
      $fecha_final=$request->fecha_final;
      $Megaquery=DB::connection($connection)->table('empleado_tarea')
=======
      $fecha_inicial=$request->fecha_inicial;
      $fecha_final=$request->fecha_final;

      //$query=DB::

      $Megaquery=DB::table('empleado_tarea')
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      ->join('tarea','id_tarea','=','tarea.id')
      ->whereBetween('fecha',array($fecha_inicial,$fecha_final))
      ->join('empleado','id_empleado','=','empleado.id')
      ->select('empleado.nombre as nombre','empleado.apellidos as apellidos',DB::raw('SUM(empleado_tarea.costo_individual) as total'))
      ->groupBy('empleado.nombre', 'empleado.apellidos')
      ->get();
<<<<<<< HEAD
      $DailyWork=DB::connection($connection)->select('SELECT
=======


      $DailyWork=DB::select('SELECT
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
        date,
        result.nombre,
        result.total

        FROM
        generate_series(
        ?::date,
        ?::date,
        ?) AS date
        LEFT OUTER JOIN
        (select date_trunc(?, t.fecha) as day,
        sum(costo_individual) as total,
        e.nombre as nombre
        from empleado_tarea as et
        join empleado as e
        on et.id_empleado=e.id
        join tarea as t
        on t.id=et.id_tarea
        group by e.nombre, day) as result
        ON (result.day=date)',array($fecha_inicial,$fecha_final,"1 day","day"));
<<<<<<< HEAD
        $Daily=DB::connection($connection)->select('SELECT
=======
        //dd($DailyWork,$Megaquery);
      $Daily=DB::select('SELECT
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
        to_char(dias, ?) AS mostrar,
        dias
        FROM
        generate_series(
        ?::date,
        ?::date,
        ?) AS dias',array("DD-MM-YYYY",$fecha_inicial,$fecha_final,"1 day"));

<<<<<<< HEAD
      $Semana=array();
      foreach ($Megaquery as $m) {
=======

      $Semana=array();
      foreach ($Megaquery as $m) {

>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
        foreach ($Daily as $k) {
          $t=0;
          foreach ($DailyWork as $d) {
            if ($d->nombre==$m->nombre&& $k->dias==$d->date&&$d->nombre!=null) {
              $t=$d->total;
            }
          }
          $e = [$m->nombre=>$t];
          array_push($Semana,$e);
          }
        }
<<<<<<< HEAD
        return view('reportes.empleados', compact('Megaquery','Semana','Daily'));
      }
      /*Para generar el reporte de los empleados en un lapso definido*/
      public function showEmpleadosLapse(Request $request)
      {
        $connection =  session()->get('key');
        $fecha_inicial=$request->fecha_inicial;
        $fecha_final=$request->fecha_final;
        $Megaquery=DB::connection($connection)->table('empleado_tarea')
=======

        //dd($Semana);

        return view('reportes.empleados', compact('Megaquery','Semana','Daily'));

      }
      /*Para generar el reporte de los empleados en un lapso definido*/

      public function showEmpleadosLapse(Request $request)
      {
        $fecha_inicial=$request->fecha_inicial;
        $fecha_final=$request->fecha_final;

        $Megaquery=DB::table('empleado_tarea')
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
        ->join('tarea','id_tarea','=','tarea.id')
        ->whereBetween('fecha',array($fecha_inicial,$fecha_final))
        ->join('empleado','id_empleado','=','empleado.id')
        ->select('empleado.nombre as nombre', 'empleado.apellidos as apellidos',DB::raw('SUM(empleado_tarea.costo_individual) as total'))
        ->groupBy('empleado.nombre', 'empleado.apellidos')
        ->get();
<<<<<<< HEAD
        return view('reportes.lapse', compact('Megaquery'));
=======

        return view('reportes.lapse', compact('Megaquery'));

>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      }
      /*Para generar el reporte de los insumos en un lapso definido*/
      public function showInsumos(Request $request)
      {
<<<<<<< HEAD
        $connection =  session()->get('key');
        $fecha_inicial=$request->fecha_inicial;
        $fecha_final=$request->fecha_final;
        $Megaquery=DB::connection($connection)->table('tarea_insumo')
=======
        $fecha_inicial=$request->fecha_inicial;
        $fecha_final=$request->fecha_final;

        $Megaquery=DB::table('tarea_insumo')
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
        ->join('tarea','id_tarea','=','tarea.id')
        ->whereBetween('fecha',array($fecha_inicial,$fecha_final))
        ->join('insumo','id_insumo','=','insumo.id')
        ->select('insumo.descripcion as nombre',DB::raw('SUM(tarea_insumo.cantidad_insumo) as cantidad'),
          DB::raw('SUM(tarea_insumo.costo_insumo) as costo'))
        ->groupBy('insumo.descripcion')
        ->get();
<<<<<<< HEAD
        return view('reportes.supplies', compact('Megaquery'));
      }

      public function showTareas(Request $request)
      {
        $connection =  session()->get('key');
        $fecha_inicial=$request->fecha_inicial;
        $fecha_final=$request->fecha_final;
        $tareas=DB::connection($connection)->table('tarea')
        ->whereBetween('fecha',array($fecha_inicial,$fecha_final))
        ->whereNull('deleted_at')->get();
        $Productos = Producto::on($connection)->get();
        $table_lote_tarea=Lote_Tarea::on($connection)->get();
        $Lotes=Lote::on($connection)->get();
        $Empleados=Empleado::on($connection)->get();
        $Labores=Labor::on($connection)->get();
        $table_empleado_tarea=Empleado_Tarea::on($connection)->get();
        $Insumos=Insumo::on($connection)->get();
        $table_tarea_insumo=Tarea_Insumo::on($connection)->get();
=======

        return view('reportes.supplies', compact('Megaquery'));

      }
      public function showTareas(Request $request)
      {
        $fecha_inicial=$request->fecha_inicial;
        $fecha_final=$request->fecha_final;
        $tareas=DB::table('tarea')
        ->whereBetween('fecha',array($fecha_inicial,$fecha_final))
        ->whereNull('deleted_at')->get();

        $Labores = Labor::all();
        $Productos = Producto::all();
        $table_lote_tarea=Lote_Tarea::all();
        $Lotes=Lote::all();
        $Empleados=Empleado::all();
        $table_empleado_tarea=Empleado_Tarea::all();
        $Insumos=Insumo::all();
        $table_tarea_insumo=Tarea_Insumo::all();

>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
        $this->getInfo($request);
        return view('reportes.index',
        compact('tareas','Labores','Productos','table_lote_tarea','Lotes','table_empleado_tarea','Insumos','table_tarea_insumo','Empleados'));
      }
<<<<<<< HEAD

=======
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      /*Me dirige a la vista de reportes de empleados*/
      public function indexEmpleados(Request $request)
      {
        return view('reportes.indexempleados');
      }
<<<<<<< HEAD
      /*Me dirige a la vista de reportes de tareas por lapso*/
=======
/*Me dirige a la vista de reportes de tareas por lapso*/
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      public function indexTarea(Request $request)
      {
        return view('reportes.indextareas');
      }
      /*Me dirige a la vista de reportes de empleados por lapso*/
      public function indexEmpleadosLapse()
      {
        return view('reportes.indexlapse');
      }
      /*Me dirige a la vista de reportes de insumos*/
      public function indexInsumos()
      {
        return view('reportes.indexsupplies');
      }

      public function getInfo(Request $request)
      {
       $fecha_inicial=$request->fecha_inicial;
       $fecha_final=$request->fecha_final;
       $tareas=DB::table('tarea')
       ->whereBetween('fecha',array($fecha_inicial,$fecha_final))
       ->get();
       $tareasJ = json_encode(((array) $tareas), true);
       $productos = Producto::all();
       $productosJ = json_encode(((array) $productos), true);
       $labores=Labor::all();
       $laboresJ = json_encode(((array) $labores), true);
       $table_lote_tarea=Lote_Tarea::all();
       $table_lote_tareaJ = json_encode(((array) $table_lote_tarea), true);
       $lotes=Lote::all();
       $lotesJ = json_encode(((array) $lotes), true);
<<<<<<< HEAD
       $Empleados=Empleado::all();
       $table_empleado_tarea=Empleado_Tarea::all();
=======

       $Empleados=Empleado::all();
       $table_empleado_tarea=Empleado_Tarea::all();

>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
       $Insumos=Insumo::all();
       $table_tarea_insumo=Tarea_Insumo::all();
       $data=array($tareasJ,$productosJ,$laboresJ,$table_lote_tareaJ,$lotesJ);
       return  json_encode($data);
     }

   }
