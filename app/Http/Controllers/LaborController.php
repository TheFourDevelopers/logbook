<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Labor;
use Validator;
class LaborController extends Controller
{

<<<<<<< HEAD

  public function __construct(){
    $this->middleware('auth');

    }
=======
  public function __construct(){
$this->middleware('auth');

}
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
<<<<<<< HEAD
     $connection = session()->get('key');
    $labores = Labor::on($connection)->get();
=======
    $labores = Labor::all();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    return view('labores.index', compact('labores'));
  }
  /**
   * Funcion que valida los campos y la bd
   */
  public function validator(Request $request)
  {
    $validator = Validator::make($request->all(), [
        'descripcion' => 'required|max:25|unique:labor,descripcion'
    ]);
    return $validator;
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
<<<<<<< HEAD
    $connection = session()->get('key');
    $labores = Labor::on( $connection)->get();
=======
    $labores = Labor::all();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    return view('labores.create',compact('labores'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
<<<<<<< HEAD
    $connection = session()->get('key');
    $labor=Labor::on( $connection)->withTrashed()->where('descripcion', $request->descripcion)->first();
=======
    $labor=Labor::withTrashed()->where('descripcion', $request->descripcion)->first();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    if ($labor!=null && $labor->trashed())
    {
        $labor->restore();
        return redirect('labores');
    }
    $labor = new Labor();
    if ($this->validator($request)->fails())
    {
        return redirect('labores/create')
        ->withErrors($this->validator($request))->withInput();
    }
<<<<<<< HEAD
    $labor::on( $connection)->create($request->all());
=======
    $labor->create($request->all());
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    return redirect('labores');
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
   public function edit($id)
   {
<<<<<<< HEAD
     $connection = session()->get('key');
     $labor  = Labor::on( $connection)->find($id);
=======
     $labor  = Labor::find($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
     return view('labores.edit',compact('labor'));
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
<<<<<<< HEAD
      $connection = session()->get('key');
      $labor = Labor::on( $connection)->findOrFail($id);
=======
      $labor = Labor::findOrFail($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      $validator = Validator::make($request->all(), [
        'descripcion' => 'required|max:25|unique:labor,descripcion,'.$id
      ]);
      if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
      }
      $labor->update($request->all());
      return redirect('labores');
    }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
<<<<<<< HEAD
    $connection = session()->get('key');
    $labor = Labor::on( $connection)->find($id);
=======
    $labor = Labor::find($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    $labor->delete();
    return redirect('labores');
  }
}
