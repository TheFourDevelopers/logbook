<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Unidad;
use Validator;
use Illuminate\Support\Facades\Redirect;
class UnidadController extends Controller
{
      public function __construct()
      {
      $this->middleware('auth');
      }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $connection =  session()->get('key');
      $unidades = Unidad::on( $connection)->get();
      return view('unidades.index', compact('unidades'));
    }
    /**
     * Funcion que valida los campos y la bd
     */
    public function validator(Request $request)
    {
      $validator = Validator::make($request->all(), [
          'nombre' => 'required|max:40|unique:unidades,nombre',
          'siglas' => 'required|max:10'
      ]);
      return $validator;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('unidades.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $connection =  session()->get('key');
      $unidad=Unidad::on( $connection)->withTrashed()->where('nombre', $request->nombre)->first();
      if ($unidad!=null && $unidad->trashed())
      {
          $unidad->restore();
          $unidad->update($request->all());
          return redirect('unidad');
      }
      $unidad = new Unidad();
      if ($this->validator($request)->fails())
      {
          return redirect('unidad/create')
          ->withErrors($this->validator($request))->withInput();
      }
      $unidad::on( $connection)->create($request->all());
      return redirect('unidad');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $connection =  session()->get('key');
      $unidad  = Unidad::on( $connection)->find($id);
      return view('unidades.edit',compact('unidad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $connection =  session()->get('key');
      $unidad = Unidad::on( $connection)->findOrFail($id);
      $validator = Validator::make($request->all(), [
        'nombre' => 'required|max:25|unique:unidades,nombre,'.$id,
        'siglas' => 'required',
      ]);
      if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
      }
      $unidad->update($request->all());
      return redirect('unidad');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

      $connection =  session()->get('key');
      $unidad = Unidad::on( $connection)->find($id);
      if (count($unidad->insumos)>0) {
        return Redirect::back()->with('message', 'Unidad no se puede eliminar , ya que se encuentra asociada con algunos insumos');
      }else {
        $unidad->delete();
        return redirect('unidad');
      }

    }
}
