<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Producto;
use Validator;
class ProductoController extends Controller
{

  public function __construct(){
<<<<<<< HEAD
    $this->middleware('auth');
  }
=======
$this->middleware('auth');

}
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
<<<<<<< HEAD
    $connection =  session()->get('key');
    $productos = Producto::on( $connection)->get();
=======
    $productos = Producto::all();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    return view('productos.index', compact('productos'));
  }

  /**
   * Funcion que valida los campos y la bd
   */
  public function validator(Request $request)
  {
    $validator = Validator::make($request->all(), [
        'nombre' => 'required|max:25|unique:producto,nombre'
    ]);
    return $validator;
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
<<<<<<< HEAD
    return view('productos.create');
=======
    $productos = Producto::all();
    return view('productos.create',compact('productos'));
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
<<<<<<< HEAD
    $connection =  session()->get('key');
    $product=Producto::on( $connection)->withTrashed()->where('nombre', $request->nombre)->first();
=======
    $product=Producto::withTrashed()->where('nombre', $request->nombre)->first();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    if ($product!=null && $product->trashed())
    {
        $product->restore();
        return redirect('productos');
    }
    $producto = new Producto();
    if ($this->validator($request)->fails())
    {

        return redirect('productos/create')
        ->withErrors($this->validator($request))->withInput();
    }
<<<<<<< HEAD
    $producto::on( $connection)->create($request->all());
=======
    $producto->create($request->all());
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    return redirect('productos');
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
   public function edit($id)
   {
<<<<<<< HEAD
     $connection =  session()->get('key');
     $producto = producto::on( $connection)->find($id);
=======
     $producto  = producto::find($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
     return view('productos.edit',compact('producto'));
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, $id)
   {
<<<<<<< HEAD
     $connection =  session()->get('key');
 		$producto = Producto::on( $connection)->findOrFail($id);
=======
 		$producto = Producto::findOrFail($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
 		$validator = Validator::make($request->all(), [
         'nombre' => 'required|max:25|unique:producto,nombre,'.$id,
     ]);
 		if ($validator->fails()) {
 			return redirect()->back()->withErrors($validator)->withInput();
 		}
 		$producto->update($request->all());
 		return redirect('productos');
   }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
<<<<<<< HEAD
    $connection =  session()->get('key');
    $producto = Producto::on( $connection)->find($id);
=======
    $producto = Producto::find($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    $producto->delete();
    return redirect('productos');
  }
}
