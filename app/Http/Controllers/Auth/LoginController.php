<?php

namespace App\Http\Controllers\Auth;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Redirect;
use App\User;
use DB;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
      
        switch ($request->company) {

          case 'a':
          session()->put('key', 'pgsql');
            break;

          case 'b':
          session()->put('key', 'pgsql1');
            break;

          default:
          session()->put('key', 'pgsql2');
            break;
        }

          if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
          return redirect()->intended('home');
        }else {
            return Redirect::back()->withInput()->withErrors(array('email' => "Verificar usuario o contraseña"));
        }
    }
}
