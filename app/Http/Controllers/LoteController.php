<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lote;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
class LoteController extends Controller
{

	public function __construct(){
<<<<<<< HEAD
		$this->middleware('auth');

		}
=======
$this->middleware('auth');

}
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
<<<<<<< HEAD
		$connection = session()->get('key');
		$lotes = Lote::on( $connection)->get();
=======
		$lotes = Lote::all();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
		return view('lote.index', compact('lotes'));
	}

	/**
   * Funcion que valida los campos y la bd
   */
  public function validator(Request $request)
  {
    $validator = Validator::make($request->all(), [
        'descripcion' => 'required|max:25',
				'ubicacion'   => 'required',
				'superficie'=>'required',
    ]);
    return $validator;
  }
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('lote.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
<<<<<<< HEAD
		$connection = session()->get('key');
		$lote=Lote::on( $connection)->withTrashed()->where('descripcion', $request->descripcion)->first();
=======
		$lote=Lote::withTrashed()->where('descripcion', $request->descripcion)->first();
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c

		if ($lote!=null && $lote->trashed())
		{
				$lote->restore();
				return redirect('lote');
		}
		$lote = new Lote();
    if ($this->validator($request)->fails())
    {
        return redirect('lote/create')
        ->withErrors($this->validator($request))->withInput();
    }
<<<<<<< HEAD
    $lote::on( $connection)->create($request->all());
=======
    $lote->create($request->all());
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    return redirect('lote');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
<<<<<<< HEAD
		$connection = session()->get('key');
		$lote = Lote::on( $connection)->findOrFail($id);
=======
		$lote = Lote::findOrFail($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
		return view('lote.edit', compact('lote'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
<<<<<<< HEAD
		$connection = session()->get('key');
		$lote = Lote::on( $connection)->findOrFail($id);

		$validator = Validator::make($request->all(), [
        'descripcion' => 'required|max:25|unique:lote,descripcion,'.$id,
				'ubicacion'   => 'required',

=======
		$lote = Lote::findOrFail($id);
		
		$validator = Validator::make($request->all(), [
        'descripcion' => 'required|max:25|unique:lote,descripcion,'.$id,
				'ubicacion'   => 'required',
				
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
    ]);
		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator)->withInput();
		}
		$lote->update($request->all());
		return redirect('lote');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
<<<<<<< HEAD
		$connection = session()->get('key');
		$lote = Lote::on( $connection)->find($id);
		$lote->delete();
=======
		Lote::destroy($id);
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
		return redirect('lote');
	}

}
