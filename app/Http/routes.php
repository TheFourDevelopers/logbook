<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::post('tareas/create','TareaController@save');
Route::post('tareas/{id}/edit','TareaController@update2');
Route::get('obtenerInsumos/{id}','TareaController@getSuppliesByid');

Route::resource('tareas/report','TareaController@report');
Route::resource('lote', 'LoteController');
Route::resource('tareas', 'TareaController');
Route::resource('empleados', 'EmpleadoController');
Route::resource('insumos', 'InsumoController');
Route::resource('productos', 'ProductoController');
Route::resource('labores', 'LaborController');
Route::resource('categoria_empleado', 'Categoria_empleadoController');
Route::resource('categoria_insumos', 'Categoria_insumosController');

Route::get('reporte_empleados','ReporteController@indexEmpleados');
Route::post('reporte_empleados','ReporteController@showEmpleados');


Route::get('reporte_empleados_lapse','ReporteController@indexEmpleadosLapse');
Route::post('reporte_empleados_lapse','ReporteController@showEmpleadosLapse');

Route::get('reporte_insumos','ReporteController@indexInsumos');
Route::post('reporte_insumos','ReporteController@showInsumos');

Route::get('reporte_tarea','ReporteController@indexTarea');
Route::post('reporte_tarea','ReporteController@showTareas');

Route::post('getLabors','ReporteController@showLabores');


Route::get('insumos/{id}/recharge','InsumoController@recharge');
Route::post('insumos/save_recharge/{id}','InsumoController@save_recharge');

Route::get('dropdown', function(){
	$id = Request::input('option');
	$insumos = Categoria_Insumo::find($id)->insumos;
	return $insumos->lists('id', 'descripcion');
});
Route::resource('usuario','UsuarioController@updatePassword');
Route::resource('usuarios','UsuarioController');

Route::get('getSupplies/{id}','TareaController@getDataTableSupplies');
Route::get('getEmployees/{id}','TareaController@getEmployees');
//Para llenar tabla de tareas

Route::post('getInfo','ReporteController@create');

Route::post('obtenerReporte','TareaController@getInfo');

// Authentication routes...

Route::get('/', [
	'uses'=>'Auth\AuthController@index',
	'as' => 'login_principal'
	]);

Route::get('login', [
	'uses'=>'Auth\AuthController@getLogin',
	'as' => 'login'
	]);

Route::post('login', 'Auth\AuthController@postLogin');

Route::get('logout', [
	'uses'=>'Auth\AuthController@getLogout',
	'as'=>'logout'
	]);
/*/// Authentication routes...
Route::get('/', [
	'uses'=>'Auth\AuthController@index',
	'as' => 'login_principal'
	]);

Route::get('login', function()
{

	if (Auth::check())
	{
		return redirect ('home');

	}
	//return redirect('login_principal');
	return view('auth.login');

});


Route::post('login', 'Auth\AuthController@postLogin');

Route::get('logout', [
	'uses'=>'Auth\AuthController@getLogout',
	'as'=>'logout'
	]);
*/
// Registration routes...
Route::get('register', [
	'uses'=>'UsuarioController@create',
	'as'=>'register'
	]);

Route::get('home', [
	'uses'=>'HomeController@index',
	'as' => 'home'
	]);
