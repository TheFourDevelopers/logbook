<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Empleado_Tarea extends Model
{
	use SoftDeletes;
    protected $table = 'empleado_tarea';
    protected $fillable = ['id_empleado','id_tarea','costo_individual'];
    public $timestamps = false;
    protected $dates = ['deleted_at'];
}
