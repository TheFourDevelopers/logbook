<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTime;
class Tarea extends Model
{
  use SoftDeletes;
  protected $table = 'tarea';
  protected $fillable = ['fecha', 'costo_mano_obra', 'id_labor','id_producto','observaciones'];
  public $timestamps = false;
  protected $dates = ['deleted_at'];

  public function scopeCreatedLastXDays($query, $nb)
  {
    return $query->where('fecha', '>=', new DateTime('-'.$nb.' days'));
  }
}
