<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Producto extends Model
{
use SoftDeletes;
    protected $table = 'producto';
    protected $fillable = ['id','nombre'];
    public $timestamps = false;
    protected $dates = ['deleted_at'];    
}
