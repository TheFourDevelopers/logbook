<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Tarea_Insumo extends Model
{
    protected $table = 'tarea_insumo';
    protected $fillable = ['id_insumo','id_tarea','cantidad_insumo'];
    public $timestamps = false;
}