<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Unidad extends Model
{
  use SoftDeletes;
    protected $table = 'unidades';
    protected $fillable = ['nombre','siglas'];
    public $timestamps = false;
    protected $dates = ['deleted_at'];

    /**
    * Get the insumos.
    */
   public function insumos()
   {
       return $this->hasMany('App\Insumo', 'id_unidad');
   }
}
