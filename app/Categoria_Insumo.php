<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Categoria_Insumo extends Model
{
	use SoftDeletes;
    protected $table = 'categoria_insumo';
    protected $fillable = ['descripcion'];
    public $timestamps = false;
    protected $dates = ['deleted_at'];

    public function insumos() {
		return $this->hasMany('Insumo');
	}
}
