<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Empleado extends Model
{

	use SoftDeletes;
    protected $table = 'empleado';
    protected $fillable = ['cedula', 'nombre', 'apellidos','domicilio','id_categoria_empleado','telefono_casa', 'telefono_personal'];
    public $timestamps = false;
    protected $dates = ['deleted_at'];
}
