<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Lote_Tarea extends Model
{
	use SoftDeletes;
    protected $table = 'lote_tarea';
    protected $fillable = ['id_lote','id_tarea'];
    public $timestamps = false;
    protected $dates = ['deleted_at'];
}
