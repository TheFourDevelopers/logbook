@extends('layouts.master')
@section('content')
<div class="panel panel-default col-md-7 col-md-offset-2 pa">
  <div class="panel-heading">Editar Insumo</div>
  <div class="panel-body">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <ul>
        <li>La descripción esta en uso, intente de nuevo.</li>
      </ul>
    </div>
    @endif
    <form action='/insumos/{{$insumo->id}}' method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="_method" value="PUT">
<<<<<<< HEAD
      <input type="hidden" name="unidad_medida" value="1">
=======
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      <label for="descripcion">Descripción:</label>
      <input type="text" class="form-control" required="required" name="descripcion" value="{{$insumo->descripcion}}">
      <br>
      <label for="unidad_medida">Unidad de medida:</label>
<<<<<<< HEAD
      <select name="id_unidad" class="form-control" required="required">
        @foreach ($unidades as $unidad)
        @if($unidad->id === $insumo->unidad->id)
        <option value="{{$insumo->unidad->id}}" selected>{{$unidad->nombre}}</option>
        @else
        <option value="{{$unidad->id}}" >{{$unidad->nombre}}</option>
        @endif
        @endforeach
=======
      <select name="unidad_medida" class="form-control" required="required" >
        <option value="Kilogramos">Kilogramos</option>
        <option value="Litros">Litros</option>
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      </select>
      <br>
      <label for="precio_compra">Precio de compra unitario:</label>
      <input type="number" step="any" name="precio_compra" class="form-control" required="required" value="{{$insumo->precio_compra}}" step="any" min="1" />
      <br>
      <label for="id_categoria_insumo">Categoria de insumo:</label>
      <select name="id_categoria_insumo" class="form-control" required="required">
        @foreach ($categoria as $cat)
        @if($cat->id === $insumo->id_categoria_insumo)
        <option value="{{$insumo->id_categoria_insumo}}" selected>{{$cat->descripcion}}</option>
        @else
        <option value="{{$cat->id}}" >{{$cat->descripcion}}</option>
        @endif
        @endforeach
      </select>
      <br>
<<<<<<< HEAD
      <a href="{{ URL::asset('/insumos') }}"><button type="button"class="btn btn-warning col-md-4 pull-right" >Volver</button></a>
      {!! Form::submit('Modificar', ['class' => 'btn btn-primary col-md-4 pull-right']) !!}
    </form>
  </div>

=======
      {!! Form::submit('Modificar', ['class' => 'btn btn-primary col-md-4 pull-right']) !!}
    </form>
  </div>
  <div class="col-xs-12">
    <a href="{{ URL::asset('/insumos') }}"><button class="btn btn-warning col-md-4 pull-right" >Volver</button></a>
  </div>
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
</div>
@endsection
