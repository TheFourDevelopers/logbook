@extends('layouts.master')
@section('content')
<div class="panel panel-default col-md-7 col-md-offset-2 pa">
  <div class="panel-heading">Recargar Insumo</div>
  <div class="panel-body">
    <form action='/insumos/save_recharge/{{$insumo->id}}' method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">

      <label for="descripcion">Descripción:</label>
      <input type="text" readonly="" class="form-control" required="required" name="descripcion" value="{{$insumo->descripcion}}">
      <br>
      <label  data-toggle="{{$insumo->unidad_medida}}" for="cantidad_recarga">Cantidad a recargar:</label>
      <input type="number" step="any" title="{{$insumo->unidad_medida}}"   name="cant_recargar" class="form-control" required="required" step="any" min="1">
      <br>

      <label for="cantidad_disponible">Cantidad disponible:</label>
      <input readonly="" type="number" step="any" name="cant_disponible" class="form-control" required="required" value="{{$insumo->cantidad_disponible}}">
      <br>
<<<<<<< HEAD
      <a href="{{ URL::asset('/insumos') }}"><button type="button"class="btn btn-warning col-md-4 pull-right" >Volver</button></a>
      {!! Form::submit('Recargar', ['class' => 'btn btn-primary col-md-4 pull-right']) !!}
    </form>
  </div>

=======
      {!! Form::submit('Recargar', ['class' => 'btn btn-primary col-md-4 pull-right']) !!}
    </form>
  </div>
  <div class="col-xs-12">
    <a href="{{ URL::asset('/insumos') }}"><button class="btn btn-warning col-md-4 pull-right" >Volver</button></a>
  </div>
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
</div>
@endsection
