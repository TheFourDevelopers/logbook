@extends('layouts.master')
@section('style')
<link rel="stylesheet" type="text/css" href="/assets/DataTables/media/css/jquery.dataTables.min.css">
@endsection()
@section('content')
<div class="panel panel-default">
  <div class="panel-heading pull-right"><a href="{{ route('insumos.create') }}" class="btn btn-primary">Agregar nuevo Insumo</a></div>
  <div class="panel-heading">Mantenimiento Insumo</div>
  <div class="panel-body">
    <table id="example" class="display" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>Descripción</th>
          <th>Unidad de medida</th>
          <th>Precio de compra</th>
          <th>Cantidad disponible</th>
          <th>Categoria de insumo</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th>Descripción</th>
          <th>Unidad de medida</th>
          <th>Precio de compra unitario</th>
          <th>Cantidad disponible</th>
          <th>Categoria de insumo</th>
          <th>Acciones</th>
        </tr>
      </tfoot>
      <tbody>
        @foreach ($insumos as $ins)
        <tr>
          <td>{{$ins->descripcion}}</td>
<<<<<<< HEAD
          <td>{{$ins->unidad->nombre}}</td>
=======
          <td>{{$ins->unidad_medida}}</td>
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
          <td>{{$ins->precio_compra}}</td>
          <td>{{$ins->cantidad_disponible}}</td>
          <td>@foreach ($categorias as $categoria)
            @if($categoria->id === $ins->id_categoria_insumo)
            {{$categoria->descripcion}}
            @endif
            @endforeach
          </td>
            <td>
              <a href='/insumos/{{$ins->id}}/edit'><button class="btn btn-primary" data-toggle="tooltip" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button></a>
              <a href='/insumos/{{$ins->id}}/recharge'><button class="btn btn-primary" data-toggle="tooltip" title="Recargar"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span></button></a>
              <form action="/insumos/{{$ins->id}}" method="POST" style="display:inline">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class="btn btn-danger" title="Eliminar">
                  <i class="glyphicon glyphicon-trash"></i>
                </button>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  @endsection
  @section('table')
  <script src="/assets/DataTables/media/js/jquery.dataTables.js"></script>
  <script src="/assets/js/table.js"></script>
  @endsection
