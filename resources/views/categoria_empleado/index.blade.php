@extends('layouts.master')
@section('style')
<link rel="stylesheet" type="text/css" href="/assets/DataTables/media/css/jquery.dataTables.min.css">
@endsection()
@section('content')
<div class="panel panel-default">
  <div class="panel-heading pull-right"><a href="{{ route('categoria_empleado.create') }}" class="btn btn-primary">Agregar nueva categoria</a></div>
  <div class="panel-heading">Mantenimiento Categoria Empleado</div>
  <div class="panel-body">
    <table id="example" class="display" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>Descripción</th>
          <th>Monto x Hora</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th>Descripción</th>
          <th>Monto</th>
          <th>Acciones</th>
        </tr>
      </tfoot>
      <tbody>
        {{-- */$x=0;/* --}}
        @foreach($categoria_empleados as $item)
        {{-- */$x++;/* --}}
        <tr>
          <td>{{ $item->descripcion }}</td><td>{{ $item->monto_x }}</td>
          <td>
            <a href="{{ route('categoria_empleado.edit', $item->id) }}">
              <button class="btn btn-primary" data-toggle="tooltip" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button></a>
              {!! Form::open([
              'method'=>'DELETE',
              'route' => ['categoria_empleado.destroy', $item->id],
              'style' => 'display:inline'
              ]) !!}
              <button type="submit" class="btn btn-danger" title="Eliminar">
                <i class="glyphicon glyphicon-trash"></i>
              </button>
              {!! Form::close() !!}
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  @endsection
  @section('table')
  <script src="/assets/DataTables/media/js/jquery.dataTables.js"></script>
  <script src="/assets/js/table.js"></script>
  @endsection
