@extends('layouts.master')
@section('content')
<div class="panel panel-default col-md-7 col-md-offset-2 pa">
	<div class="panel-heading">Editar Empleado</div>
	<div class="panel-body">
		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<ul>
				<li>La cédula esta en uso, intente de nuevo.</li>
			</ul>
		</div>
		@endif
		<form action='/empleados/{{$empleado->id}}' method="POST">
			<input type="hidden" name="_method" value="PUT">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<label for="cedula">Cédula:</label>
			<input class="form-control" required="required" name="cedula" type="number" id="cedula" value="{{$empleado->cedula}}">
			<br>
			<label for="nombre">Nombre completo:</label>
			<input class="form-control" required="required" type="text" name="nombre"  id="nombre" value="{{$empleado->nombre}}">
			<br>
			<label for="apellidos">Apellidos:</label>
			<input class="form-control" required="required" type="text" name="apellidos"  id="apellidos" value="{{$empleado->apellidos}}">
			<br>
			<label for="domicilio">Domicilio:</label>
			<input class="form-control" required="required" type="text" name="domicilio"  id="domicilio" value="{{$empleado->domicilio}}">
			<br>
			<label for="id_categoria_empleado">Categoria de empleado:</label>
			<select name="id_categoria_empleado"  class="form-control">
				@foreach ($categoria as $cat)
				@if($cat->id === $empleado->id_categoria_empleado)
				<option value="{{$empleado->id_categoria_empleado}}" selected>{{$cat->descripcion}}</option>
				@else
				<option value="{{$cat->id}}" >{{$cat->descripcion}}</option>
				@endif
				@endforeach
			</select>
			<br>
			<label for="telefono_casa">Telefono casa:</label>
			<input class="form-control"  type="number" name="telefono_casa" id="telefono_casa" value="{{$empleado->telefono_casa}}">
			<br>
			<label for="telefono_personal">Telefono personal:</label>
			<input class="form-control"  type="number" name="telefono_personal" value="{{$empleado->telefono_personal}}">
			<br>
<<<<<<< HEAD
			<a href="{{ URL::asset('/empleados') }}"><button type="button" class="btn btn-warning col-md-4 pull-right" >Volver</button></a>
			{!! Form::submit('Guardar', ['class' => 'btn btn-primary col-md-4 pull-right']) !!}
		</form>
	</div>

=======
			<br>
			{!! Form::submit('Edit', ['class' => 'btn btn-primary col-md-4 pull-right']) !!}
		</form>
	</div>
	<div class="col-xs-12">
		<a href="{{ URL::asset('/empleados') }}"><button class="btn btn-warning col-md-4 pull-right" >Volver</button></a>
	</div>
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
</div>
@endsection
