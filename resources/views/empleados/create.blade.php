@extends('layouts.master')
@section('content')
<div class="panel panel-default col-md-8 col-md-offset-2 pa">
	<div class="panel-heading">Nuevo Empleado</div>
	<div class="panel-body">
		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<ul>
				<li>La cédula esta en uso, intente de nuevo.</li>
			</ul>
		</div>
		@endif
		<form class="form-horizontal" action='/empleados' method="POST" >
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<label for="cedula">Cédula:</label>
			<input class="form-control" required="required" name="cedula" type="text" id="cedula" maxlength="16">
			<br>
			<label for="nombre">Nombre completo:</label>
			<input class="form-control" required="required" type="text" name="nombre"  id="nombre">
			<br>
			<label for="apellidos">Apellidos:</label>
			<input class="form-control" required="required" type="text" name="apellidos"  id="apellidos">
			<br>
			<label for="domicilio">Domicilio:</label>
			<input class="form-control" required="required" type="text" name="domicilio"  id="domicilio">
			<br>
			<label for="id_categoria_empleado">Categoria de empleado:</label>
			<select name="id_categoria_empleado"  class="form-control">
				@foreach ($categorias as $categoria)
				<option value="{{$categoria->id}}">{{$categoria->descripcion}}</option>
				@endforeach
			</select>
			<br>
			<label for="telefono_casa">Telefono casa:</label>
			<input class="form-control" value=0 type="number" name="telefono_casa" id="telefono_casa" min="0" max=99999999999>
			<br>
			<label for="telefono_personal">Telefono personal:</label>
			<input class="form-control"  value=0 type="number" name="telefono_personal" id="telefono_personal" min="0" max="99999999999">
			<br>
<<<<<<< HEAD
			<a href="{{ URL::asset('/empleados') }}"><button type="button" class="btn btn-warning col-md-4 pull-right" >Volver</button></a>
			{!! Form::submit('Crear', ['class' => 'btn btn-primary col-md-4 pull-right']) !!}
		</form>
	</div>

=======
			{!! Form::submit('Crear', ['class' => 'btn btn-primary col-md-4 pull-right']) !!}
		</form>
	</div>
	<div class="col-xs-12">
		<a href="{{ URL::asset('/empleados') }}"><button class="btn btn-warning col-md-4 pull-right" >Volver</button></a><br>
	</div>
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
</div>
@endsection
