@extends('layouts.master')
@section('style')
<link rel="stylesheet" href="/assets/DataTables/media/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="/assets/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css">
@endsection()
@section('content')
<div class="panel panel-default">
  <div class="panel-heading pull-right"><a href="{{ route('empleados.create') }}" class="btn btn-primary">Agregar Nuevo Empleado</a></div>
  <div class="panel-heading">Mantenimiento Empleados</div>
  <div class="panel-body">

    <table id="example" class="table table-striped" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>Cédula</th>
          <th>Nombre</th>
          <th>Apellidos</th>
          <th>Domicilio</th>
          <th>Categoria</th>
          <th>Tel-Casa</th>
          <th>Tel-Personal</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th>Cédula</th>
          <th>Nombre</th>
          <th>Apellidos</th>
          <th>Domicilio</th>
          <th>Categoria</th>
          <th>Tel-Casa</th>
          <th>Tel-Personal</th>
          <th>Acciones</th>
        </tr>
      </tfoot>
      <tbody>
        @foreach ($empleados as $emp)
        <tr>
          <td>{{$emp->cedula}}</td>
          <td>{{$emp->nombre}}</td>
          <td>{{$emp->apellidos}}</td>
          <td>{{$emp->domicilio}}</td>
          <td>@foreach ($categorias as $categoria)
            @if($categoria->id === $emp->id_categoria_empleado)
            {{$categoria->descripcion}}
            @endif
            @endforeach</td>
            <td>{{$emp->telefono_casa}}</td>
            <td>{{$emp->telefono_personal}}</td>
            <td>
              <a href='/empleados/{{$emp->id}}/edit'><button class="btn btn-primary" data-toggle="tooltip" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button></a>
              <form action="/empleados/{{$emp->id}}" method="POST" style="display:inline">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class="btn btn-danger" title="Eliminar">
                  <i class="glyphicon glyphicon-trash"></i>
                </button>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  @endsection
  @section('table')
  <script src="/assets/DataTables/media/js/jquery.dataTables.min.js"></script>
  <script src="/assets/DataTables/media/js/dataTables.bootstrap.min.js"></script>
  <script src="/assets/js/table.js"></script>
  @endsection
