@extends('layouts.master')
@section('content')
<div class="row">
  <div class="panel panel-primary filterable">
    <div class="panel-heading">
      <h3 class="panel-title">Usuarios</h3>
      <div class="pull-right">
        <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filtros</button>
      </div>
    </div>
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr class="filters">
            <th><input type="text" class="form-control" placeholder="Nombre" disabled></th>
<<<<<<< HEAD
            <th><input type="text" class="form-control" placeholder="Usuario" disabled></th>
            <th><input type="text" class="form-control" placeholder="Rol" disabled></th>
=======
            <th><input type="text" class="form-control" placeholder="Apellidos" disabled></th>
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
            <th><input type="text" class="form-control" placeholder="Acciones" disabled></th>
          </tr>
        </thead>
        <tbody>
<<<<<<< HEAD
         @foreach ($usuarios as $usuario)
         <tr>
          <td>{{$usuario->name}}</td>
          <td>{{$usuario->email}}</td>
          @if($usuario->isAdmin === 1 )
            <td>Administrador</td>
          @else
            <td>Usuario</td>
          @endif
           <td>
             <a href='/usuarios/{{$usuario->id}}/edit'><button class="btn btn-primary" data-toggle="tooltip" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button></a>
             <form action="/usuarios/{{$usuario->id}}" method="POST" style="display:inline">
=======
         @foreach ($usuarios as $ins)
         <tr>
          <td>{{$ins->name}}</td>
          <td>{{$ins->lastname}}</td>

           <td>
             <a href='/usuarios/{{$ins->id}}/edit'><button class="btn btn-primary" data-toggle="tooltip" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button></a>
             <form action="/usuarios/{{$ins->id}}" method="POST" style="display:inline">
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="DELETE">
              <button type="submit" class="btn btn-danger" title="Eliminar">
                <i class="glyphicon glyphicon-trash"></i>
              </button>
            </form>
          </td>
        </tr>
         @endforeach
      </tbody>
    </table>
  </div>

</div>
</div>
<h1><a href="{{ route('register') }}" class="btn btn-primary pull-right btn-sm">Agregar Usuario</a></h1>
@endsection
