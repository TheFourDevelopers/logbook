<<<<<<< HEAD
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('w') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Empresa</label>

                            <div class="col-md-6">
                              <select class="form-control" required name="company" id="sel1">
                                <option value="a">Empresa A</option>
                                <option value="b">Empresa B</option>
                                <option value="c">Empresa C</option>
                              </select>
                                @if ($errors->has('email'))
                                    <span class="help-block">

                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Nombre Usuario</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Iniciar Sessión
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <p>Logbook-2016</p>
        <p>Desarrollado por<strong>
        <a href="http://novlic.com/">
           Novlic.com</strong>
        </a>
        </p>
      </div>
    </div>
  </div>
</footer>
@endsection
=======

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
  <title>Inicio de sesión-Logbook</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="/assets/css/login.css" rel="stylesheet">
  <script src="/assets/js/jquery-1.12.3.min.js"></script>
  <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
  <section id="login">
    <div class="container">
     <div class="row">
       <div class="col-xs-12">
         <div class="form-wrap">
          @if(Session::has('message'))
          <div class="alert alert-info">
            {{ Session::get('message') }}
          </div>
          @endif
          <h1><img src="/assets/img/book.ico"></h1>
          @if (count($errors) > 0)
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
          <div class="panel-body">
          {!! Form::open(['route' => 'login', 'class' => 'form']) !!}
            {!! csrf_field() !!}
            <div class="form-group">
              <label for="email" class="sr-only">Email</label>
              <input type="text" name="email" id="email" class="form-control" placeholder="Nombre de usuario">
            </div>
            <div class="form-group">
              <label for="key" class="sr-only">Password</label>
              <input type="password" name="password" id="key" class="form-control" placeholder="Contraseña">
            </div>
            <div class="checkbox">
              <span class="character-checkbox" onclick="showPassword();"></span>
              <span class="label">Mostrar contraseña</span>
            </div>
            <input type="submit" id="btn-login" class="btn btn-custom btn-lg btn-block" value="Log in">
            {!! Form::close() !!}

          </div>
        </div> <!-- /.col-xs-12 -->
      </div> <!-- /.row -->
    </div> <!-- /.container -->
  </section>
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <p>Logbook-2016</p>
          <p>Hecho por<strong> TheFourDevelopers</strong></p>
        </div>
      </div>
    </div>
  </footer>
  <script src="/assets/js/login.js"></script>
</body>
</html>
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
