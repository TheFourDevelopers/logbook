@extends('layouts.master')
@section('content')
<div class="panel panel-default col-md-7 col-md-offset-2 pa">
  <div class="panel-heading">Editar Categoria Insumo</div>
  <div class="panel-body">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <ul>
        <li>La descripción esta en uso, intente de nuevo.</li>
      </ul>
    </div>
    @endif
    {!! Form::model($categoria_insumo, [
    'method' => 'PATCH',
    'route' => ['categoria_insumos.update', $categoria_insumo->id],
    'class' => 'form-horizontal'
    ]) !!}
    {!! Form::label('descripcion', 'Descripcion: ') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control','required' => 'required']) !!}
    <br>
<<<<<<< HEAD
    <a href="{{ URL::asset('/categoria_insumos') }}"><button  type="button"class="btn btn-warning col-md-4 pull-right" >Volver</button></a>
    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary col-md-4 pull-right']) !!}
    {!! Form::close() !!}
  </div>

=======
    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary col-md-4 pull-right']) !!}
    {!! Form::close() !!}
  </div>
  <div class="col-xs-12">
    <a href="{{ URL::asset('/categoria_empleado') }}"><button class="btn btn-warning col-md-4 pull-right" >Volver</button></a>
  </div>
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
</div>
@endsection
