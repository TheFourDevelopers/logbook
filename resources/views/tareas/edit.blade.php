@extends('layouts.master')
@section('content')
<div class="panel panel-default col-md-12 pa">
	<div class="panel-heading">Editar Tarea</div>
	<div class="panel-body">
		{!! Form::model($tarea, [
		'method' => 'PUT',
		'route' => ['tareas.edit', $tarea->id], 'onSubmit' => 'return Validation.filled();'
		]) !!}
		<input type="hidden"   id="myid" value="{{$tarea->id}}">
<<<<<<< HEAD
			<meta name="_token" content="{!! csrf_token() !!}" />
=======

>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
		<div class="col-md-4">
			<label for="fecha" id="fech">Fecha:</label>
			<input type="date" name="fecha" id="fecha" required="" class="form-control" value="{{$tarea->fecha}}">
			<br>
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<label for="labores">Labor:</label>
			<select required name="id_labor" id="id_labor" class="registro form-control"  onchange="Tasks.habilitar();" >
				<option selected></option>
				@foreach ($labores as $lab)
				@if($lab->id === $tarea->id_labor)
				<option value="{{$lab->id}}" selected>{{$lab->descripcion}}</option>
				@else
				<option value="{{$lab->id}}" >{{$lab->descripcion}}</option>
				@endif
				@endforeach
			</select>
			<br>
			<label for="productos">Producto:</label>
			<select required id="id_producto" name="id_producto" class="registro form-control" onchange="Tasks.habilitar();">
				<option selected></option>
				@foreach ($productos as $prod)
				@if($prod->id === $tarea->id_producto)
				<option value="{{$prod->id}}" selected>{{$prod->nombre}}</option>
				@else
				<option value="{{$prod->id}}" >{{$prod->nombre}}</option>
				@endif
				@endforeach
			</select>
			<br>
			<label for="lotes">Lote:</label>
			<select required id="id_lote" name="id_lote" class="registro form-control" onchange="Tasks.habilitar();">
				<option selected></option>
				@foreach ($lotes as $lot)

				@if($lot->id==$idLote[0]->id_lote)

				<option value="{{$lot->id}}" selected>{{$lot->descripcion}} {{$lot->ubicacion}}</option>
				@else
				<option value="{{$lot->id}}" >{{$lot->descripcion}} {{$lot->ubicacion}}</option>
				@endif
				@endforeach
			</select>
			<br>
			<label for="observaciones">Observaciones</label>
			<br>
			<textarea id="observaciones" class="form-control" >{{$tarea->observaciones}}  </textarea>
			<br>
			<br>
			<br>
		</div>
		<div class="col-md-8">
			<label for="tipo_pago">Tipo de pago:</label>
			<select name="tipo_pago" id="pago" onchange="Tasks.habilitar();" class="form-control">
				<option value=1>Por hora</option>
				<option value=2>Por destajo</option>
			</select>

			<label for="empleados_requeridos">Empleados</label>
			<select id="empleados_requeridos"  class="form-control ">
				@foreach($nombres as $empleado)
				<option  value="{{$empleado->id}}">{{$empleado->nombre}} {{$empleado->apellidos}}</option>
				@endforeach
			</select>
			<br>
			<button type="button" id="addEmployee" class="btn btn-default" data-dismiss="modal" onclick="agregarEmpleadoTabla();">Agregar</button>

			<table id="t_empleados" name="t_empleados" class="table">
				<thead>
					<tr>
						<th>Empleado</th>
						<th id="cantidad">Horas</th>
						<th id="precio">Precio</th>
					</tr>
				</thead>
				<tbody id="tb">
				</tbody>
			</table>
			<label for="t_insumos">Insumos:</label>
			<select  name="myselect" id="myselect" class="categoria form-control" onchange="cambiar_categoria();" >
				<option selected class="form-control"></option>
				@foreach ($categoria_insumos as $cat_i)
				<option value="{{$cat_i->id}}">{{$cat_i->descripcion}}</option>
				@endforeach
			</select>
			<br>
			<select id="insumos_requeridos" class="form-control">
				<option>Debe escoger una categoria primero</option>
			</select>
			<br>
			<button type="button" name="button" class="form-control" onclick="agregarInsumosTabla();">Agregar</button>
			<table id="tblInsumos" name="tblInsumos" class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Cantidad</th>
					</tr>
				</thead>
				<tbody id="tb">

				</tbody>
			</table>
		</div>
		<div class="col-xs-12">
<<<<<<< HEAD
			<a href="{{ URL::asset('/tareas') }}"><button type="button" class="btn btn-warning col-md-4 pull-right" >Volver</button></a>
			<button type="button"  class="btn btn-primary col-md-4 pull-right" onclick="update()" name="button">Guardar</button>
		</div>
		{!! Form::close() !!}

=======
			<button type="button"  class="btn btn-primary col-md-4 pull-right" onclick="update()" name="button">Guardar</button>
		</div>
		{!! Form::close() !!}
		<div class="col-xs-12">
			<a href="{{ URL::asset('/tareas') }}"><button class="btn btn-warning col-md-4 pull-right" >Volver</button></a>
		</div>
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
	</div>
</div>
@endsection
@section('table')
<script src="/assets/js/create.js"></script>
<script src="/assets/js/update.js"></script>
@endsection
