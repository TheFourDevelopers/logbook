@extends('layouts.master')
@section('style')
  <link rel="stylesheet" href="/assets/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css">
  <link rel="stylesheet" href="/assets/DataTables/media/css/dataTables.bootstrap.min.css">
@endsection
@section('content')
<div class="panel panel-default">
  <div class="panel-heading pull-right"><a href="{{ route('tareas.create') }}" class="btn btn-primary">Agregar nueva Tarea</a></div>
  <div class="panel-heading">Mantenimiento Tareas</div>
  <div class="panel-body">
    <div class="table-responsive">


    <table id="example" class="table table-striped" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>Fecha</th>
          <th>Labor</th>
          <th>Producto</th>
          <th>Lote</th>
          <th>Mano de obra</th>
          <th>Insumos</th>
          <th>Observaciones</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th>Fecha</th>
          <th>Labor</th>
          <th>Producto</th>
          <th>Lote</th>
          <th>Mano de obra</th>
          <th>Insumos</th>
          <th>Observaciones</th>
          <th>Acciones</th>
        </tr>
      </tfoot>
      <tbody>
        @foreach ($tareas as $task)
        <tr id="{{$task->id}}">
          <td><?php
          $date=date_create($task->fecha);
          echo date_format($date,"d/m/Y");
          ?>
        </td>
        <td>
          @foreach($Labores as $labor)
          @if($task->id_labor === $labor->id)
          {{$labor->descripcion}}
          @endif
          @endforeach
        </td>
        <td>
          @foreach($Productos as $prod)
          @if($task->id_producto === $prod->id)
          {{$prod->nombre}}
          @endif
          @endforeach
        </td>
        <td>
          @foreach($table_lote_tarea as $lt)
          @if($task->id === $lt->id_tarea)
          @foreach($Lotes as $lote)
          @if($lt->id_lote===$lote->id)
          {{$lote->descripcion}} {{$lote->ubicacion}}
          @endif
          @endforeach
          @endif
          @endforeach
        </td>
        <!--para mostrar el costo de la mano de obra-->
        <td><div class="btn-group">
          <button id="m{{$task->id}}" title="Maria" type="button" class="btn btn-info">{{$task->costo_mano_obra}}</button>
          <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
            <span class="caret"></span>
            <span class="sr-only"></span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <table id="te{{$task->id}}" class="costo_personal">
              <thead>
                <th>Empleado</th>
                <th>Costo</th>
              </thead>
              <tbody>
                @foreach($table_empleado_tarea as $et)
                @if($task->id == $et->id_tarea)
                @foreach($Empleados as $e)
                @if($et->id_empleado==$e->id)
                <tr>
                  <td>
                    {{$e->nombre}} {{$e->apellidos}}
                  </td>
                   <td>
                    {{$et->costo_individual}}
                  </td>
                  
                </tr>
                @endif
                @endforeach
                @endif
                @endforeach
              </tbody>
            </table>
          </ul>
        </div>
      </td>
      <!--para mostrar el costo de los insumos -->
      <td><div class="btn-group">
        <button id="b{{$task->id}}" type="button" class="btn btn-info">{{$task->costo_insumos}}</button>
        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
          <span class="caret"></span>
          <span class="sr-only"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
          <table id="t{{$task->id}}" class="costo_insumo">
            <thead>
              <th>Insumo</th>
              <th>Costo</th>
            </thead>
            <tbody>
              @foreach($table_tarea_insumo as $ti)
              @if($task->id === $ti->id_tarea)
              @foreach($Insumos as $in)
              @if($ti->id_insumo===$in->id)
              <tr>
                <td>
                  {{$in->descripcion}}
                </td>
                <td id="ti{{$task->id}}">
                  {{$ti->costo_insumo}}
                </td>
              </tr>
              @endif
              @endforeach
              @endif
              @endforeach
            </tbody>
          </table>
        </ul>
      </div>
    </td>
    <td>{{$task->observaciones}}</td>
    <td>
      <a href='/tareas/{{$task->id}}/edit'><button class="btn btn-primary" data-toggle="tooltip" title="Editar"><span class="glyphicon glyphicon-pencil" aria-			    hidden="true"></span></button></a>
        <form action="/tareas/{{$task->id}}" method="POST"  style="display:inline" onsubmit = 'return ConfirmDelete()'>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
       <input type="hidden" name="_method" value="DELETE">

        <button type="submit" class="btn btn-danger"  title="Eliminar">
          <i class="glyphicon glyphicon-trash"></i>
        </button>
      </form>
    </td>
  </tr>
  @endforeach
</tbody>
</table>
</div>
</div>
</div>

@endsection
@section('table')
<script src="/assets/DataTables/media/js/jquery.dataTables.min.js"></script>
<script src="/assets/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<!--<script src="/assets/js/table.js"></script>-->

<script src="/assets/js/confirm.js"></script>

@endsection
