@extends('layouts.master')
@section('content')
<div class="panel panel-default">
  <div class="panel-heading">Reporte por Periodo</div>
  <div class="panel-body">
    <div class="col-md-6 col-md-offset-3">
      <form action='/reporte_empleados_lapse' method="POST" onsubmit="return dates()">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <label for="fecha_inicial">Fecha inicial del reporte:</label>
        <input required type="date" name="fecha_inicial" id="fecha_inicial" max="<?php echo date("d-m-Y");?>" class="form-control" value="<?php echo isset($_POST['fecha_inicial']) ? $_POST['fecha_inicial'] : '' ?>"/>
        <br>
        <label for="fecha_final">Fecha final del reporte:</label>
        <input required type="date" name="fecha_final" id="fecha_final" class="form-control" max="<?php echo date("d-m-Y");?>"/ value="<?php echo isset($_POST['fecha_final']) ? $_POST['fecha_final'] : '' ?>">
        <br>
        {!! Form::submit('Buscar', ['class' => 'btn btn-primary col-md-4 pull-right']) !!}
      </form>
    </div>
  </div>
</div>
@endsection
@section('table')
<script type="text/javascript" src="/assets/js/otro.js"></script>
@endsection