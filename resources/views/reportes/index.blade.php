@extends('layouts.master')
@section('style')
<link rel="stylesheet" href="/assets/DataTables/media/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="/assets/DataTables/media/css/buttons.dataTables.min.css">
@endsection
@section('content')
<div class="panel panel-default">
  <div class="panel-heading" id="text">Reporte Tareas <?php
  $f_inicial = isset($_POST['fecha_inicial']) ? $_POST['fecha_inicial'] : '';
  $f_final = isset($_POST['fecha_final']) ? $_POST['fecha_final'] : '';
  $new_fi = date("d-m-Y", strtotime($f_inicial));
  $new_ff = date("d-m-Y", strtotime($f_final));
  echo $new_fi . " -- " . $new_ff;?></div>
  <div class="panel-body">
    <div class="col-md-6 col-md-offset-3">
    <form action='/reporte_tarea' method="POST" onsubmit="return dates()">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <label for="fecha_inicial">Fecha inicial del reporte:</label>
        <input required type="date" name="fecha_inicial" id="fecha_inicial" max="<?php echo date("d-m-Y");?>" class="form-control" value="<?php echo isset($_POST['fecha_inicial']) ? $_POST['fecha_inicial'] : '' ?>"/>
        <br>
        <label for="fecha_final">Fecha final del reporte:</label>
        <input required type="date" name="fecha_final" id="fecha_final" class="form-control" max="<?php echo date("d-m-Y");?>"/ value="<?php echo isset($_POST['fecha_final']) ? $_POST['fecha_final'] : '' ?>">
        <br>
        {!! Form::submit('Buscar', ['class' => 'btn btn-primary col-md-4 pull-right']) !!}
    </form>
    </div>
    <div class="col-md-12">
      <hr>
      <hr>
      <hr>
    <table id="example" class="table table-striped" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Fecha</th>
              <th>Labor</th>
              <th>Producto</th>
              <th>Lote</th>
              <th>Costo de Mano de obra</th>
              <th>Costo de Insumos</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Fecha</th>
              <th>Labor</th>
              <th>Producto</th>
              <th>Lote</th>
              <th>Costo de Mano de obra</th>
              <th>Costo de Insumos</th>
            </tr>
          </tfoot>
          <tbody>
            @foreach ($tareas as $task)

            <tr id="{{$task->id}}">
              <td><?php
              $date=date_create($task->fecha);
              echo date_format($date,"d/m/Y");
              ?>
            </td>
            <td>
              @foreach($Labores as $labor)
              @if($task->id_labor === $labor->id)
              {{$labor->descripcion}}
              @endif
              @endforeach
            </td>
            <td>
              @foreach($Productos as $prod)
              @if($task->id_producto === $prod->id)
              {{$prod->nombre}}
              @endif
              @endforeach
            </td>
            <td>
              @foreach($table_lote_tarea as $lt)
              @if($task->id === $lt->id_tarea)
              @foreach($Lotes as $lote)
              @if($lt->id_lote===$lote->id)
              {{$lote->descripcion}}
              @endif
              @endforeach
              @endif
              @endforeach
            </td>
            <!--para mostrar el costo de la mano de obra-->
            <td>
              {{$task->costo_mano_obra}}
            </td>
            <!--para mostrar el costo de los insumos -->
            <td>{{$task->costo_insumos}}</td>
          </tr>
          @endforeach
        </tbody>
      </div>
    </div>
  </div>
</table>
@endsection
@section('table')
<script type="text/javascript" src="/assets/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="/assets/DataTables/extensions/Buttons/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="/assets/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
<script type="text/javascript" src="/assets/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="/assets/DataTables/extensions/Buttons/js/buttons.colVis.js"></script>
<script type="text/javascript" src="/assets/js/table.js"></script>
<script type="text/javascript" src="/assets/js/report/jszip.min.js"></script>
<script type="text/javascript" src="/assets/js/report/pdfmake.min.js"></script>
<script type="text/javascript" src="/assets/js/report/vfs_fonts.js"></script>
<script type="text/javascript" src="/assets/js/otro.js"></script>
@endsection
