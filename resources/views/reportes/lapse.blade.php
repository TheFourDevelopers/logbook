@extends('layouts.master')
@section('style')
<link rel="stylesheet" href="/assets/DataTables/media/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="/assets/DataTables/media/css/buttons.dataTables.min.css">
@endsection
@section('content')
<div class="panel panel-default">
  <div class="panel-heading" id="text">Reporte por Periodo
    <?php
    $f_inicial = isset($_POST['fecha_inicial']) ? $_POST['fecha_inicial'] : '';
    $f_final = isset($_POST['fecha_final']) ? $_POST['fecha_final'] : '';
    $new_fi = date("d-m-Y", strtotime($f_inicial));
    $new_ff = date("d-m-Y", strtotime($f_final));
    echo $new_fi . " -- " . $new_ff;?></div>
  <div class="panel-body">
    <div class="col-md-6 col-md-offset-3">
      <form action='/reporte_empleados_lapse' method="POST" onsubmit="return dates()">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <label for="fecha_inicial">Fecha inicial del reporte:</label>
        <input required type="date" name="fecha_inicial" id="fecha_inicial" max="<?php echo date("d-m-Y");?>" class="form-control" value="<?php echo isset($_POST['fecha_inicial']) ? $_POST['fecha_inicial'] : '' ?>"/>
        <br>
        <label for="fecha_final">Fecha final del reporte:</label>
        <input required type="date" name="fecha_final" id="fecha_final" class="form-control" max="<?php echo date("d-m-Y");?>"/ value="<?php echo isset($_POST['fecha_final']) ? $_POST['fecha_final'] : '' ?>">
        <br>
        {!! Form::submit('Buscar', ['class' => 'btn btn-primary col-md-4 pull-right']) !!}
      </form>
      <!--muestra la tabla-->
      <div class="col-md-12">
        <hr>
        <hr>
        <hr>
        <table id="example" class="table table-striped" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Empleado</th>
              <th>Total</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Empleado</th>
              <th>Total</th>
            </tr>
          </tfoot>
          <tbody>
            @foreach ($Megaquery as $m)
            <tr>
              <td>{{$m->nombre}} {{$m->apellidos}}</td>
              <td>{{$m->total}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @endsection
  @section('table')
  <script type="text/javascript" src="/assets/DataTables/media/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/assets/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="/assets/DataTables/extensions/Buttons/js/buttons.flash.min.js"></script>
  <script type="text/javascript" src="/assets/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
  <script type="text/javascript" src="/assets/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
  <script type="text/javascript" src="/assets/DataTables/extensions/Buttons/js/buttons.colVis.js"></script>
  <script type="text/javascript" src="/assets/js/table.js"></script>
  <script type="text/javascript" src="/assets/js/report/jszip.min.js"></script>
  <script type="text/javascript" src="/assets/js/report/pdfmake.min.js"></script>
  <script type="text/javascript" src="/assets/js/report/vfs_fonts.js"></script>
  <script type="text/javascript" src="/assets/js/otro.js"></script>
  @endsection
