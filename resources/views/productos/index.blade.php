@extends('layouts.master')
@section('style')
<link rel="stylesheet" type="text/css" href="/assets/DataTables/media/css/jquery.dataTables.min.css">
@endsection
@section('content')
<div class="panel panel-default">
  <div class="panel-heading pull-right"><a href="{{ route('productos.create') }}" class="btn btn-primary">Agregar nuevo producto</a></div>
  <div class="panel-heading">Mantenimiento Productos</div>
  <div class="panel-body">
    <table id="example" class="table">
      <thead>
        <tr>
          <th>Descripción</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th>Descripcion</th>
          <th>Acciones</th>
        </tr>
      </tfoot>
      <tbody>
        @foreach ($productos as $producto)
        <tr>
          <td>{{$producto->nombre}}</td>
          <td>
            <a href='/productos/{{$producto->id}}/edit'><button class="btn btn-primary" data-toggle="tooltip" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button></a>
            <form action="/productos/{{$producto->id}}" method="POST"  style="display:inline">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="_method" value="DELETE">

              <button type="submit" class="btn btn-danger" title="Eliminar">
                <i class="glyphicon glyphicon-trash"></i>
              </button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
@section('table')
<script src="/assets/DataTables/media/js/jquery.dataTables.js"></script>
<script src="/assets/js/table.js"></script>
@endsection
