@extends('layouts.master')
@section('style')
<link rel="stylesheet" type="text/css" href="/assets/DataTables/media/css/jquery.dataTables.min.css">
@endsection
@section('content')
<div class="panel panel-default">
  <div class="panel-heading pull-right"><a href="{{ route('unidad.create') }}" class="btn btn-primary">Agregar nueva Unidad</a></div>
  <div class="panel-heading">Mantenimiento Unidades</div>
  @if(Session::has('message'))
      <div class="alert alert-info">
          {{ Session::get('message') }}
      </div>
  @endif
  
  <div class="panel-body">
    <table id="example" class="display" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Símbolo</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th>Nombre</th>
          <th>Símbolo</th>
          <th>Acciones</th>
        </tr>
      </tfoot>
      <tbody>
        @foreach($unidades as $unidad)
        <tr>
          <td>{{ $unidad->nombre }}</td>
          <td>{{ $unidad->siglas }}</td>
          <td>
            <a href="{{ url('/unidad/'.$unidad->id.'/edit') }}"><button class="btn btn-primary" data-toggle="tooltip" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button></a>
            {!! Form::open(['method'=>'delete','action'=>['UnidadController@destroy',$unidad->id], 'style' => 'display:inline']) !!}
            <button type="submit" class="btn btn-danger" title="Eliminar">
              <i class="glyphicon glyphicon-trash"></i>
            </button>{!! Form::close() !!}
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
@section('table')
<script src="/assets/DataTables/media/js/jquery.dataTables.js"></script>
<script src="/assets/js/table.js"></script>
@endsection
