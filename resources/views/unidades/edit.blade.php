@extends('layouts.master')
@section('content')
<div class="panel panel-default col-md-7 col-md-offset-2 pa">
  <div class="panel-heading">Editar Unidad</div>
  <div class="panel-body">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <ul>
        <li>El nombre de la unidad esta en uso, intente de nuevo.</li>
      </ul>
    </div>
    @endif
    <form action='/unidad/{{$unidad->id}}' method="POST">
      <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <label for="Descripcion">Nombre:</label>
        <input class="form-control" required="required" name="nombre" type="text"  value="{{$unidad->nombre}}">
        <label for="Ubicacion">Símbolo:</label>
        <input class="form-control" required="required" name="siglas" type="text"  value="{{$unidad->siglas}}">
        <br>
        <a href="{{ URL::asset('/unidad') }}"><button type="button" class="btn btn-warning col-md-4 pull-right" >Volver</button></a>
        {!! Form::submit('Guardar', ['class' => 'btn btn-primary col-md-4 pull-right']) !!}
    </form>
  </div>

</div>
@endsection
