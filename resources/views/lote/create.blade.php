@extends('layouts.master')
@section('content')
<div class="panel panel-default col-md-8 col-md-offset-2 pa">
<div class="panel-heading">Nuevo Lote</div>
  <div class="panel-body">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <ul>
        <li>La descripción del lote esta en uso, intente de nuevo.</li>
      </ul>
    </div>
    @endif
    <form action='/lote' method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <label for="Descripcion">Descripcion:</label>
      <input class="form-control" required="required" name="descripcion" type="text" id="descripcion">
      <label for="Ubicacion">Ubicacion:</label>
      <input class="form-control" required="required" name="ubicacion" type="text" id="ubicacion">
      <label for="Superficie">Superficie (m2)</label>
      <input class="form-control" required="required" name="superficie" type="number" id="superficie">
      <br>
<<<<<<< HEAD
      <a href="{{ URL::asset('/lote') }}"><button type="button"class="btn btn-warning col-md-4 pull-right" >Volver</button></a>
      {!! Form::submit('Crear', ['class' => 'btn btn-primary col-md-4 pull-right']) !!}
    </form>
  </div>

=======
      {!! Form::submit('Crear', ['class' => 'btn btn-primary col-md-4 pull-right']) !!}
    </form>
  </div>
  <div class="col-xs-12 back">
    <a href="{{ URL::asset('/lote') }}"><button class="btn btn-warning col-md-4 pull-right" >Volver</button></a><br>
  </div>
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
</div>
@endsection
