<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
<<<<<<< HEAD

=======
  
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Logbook</title>
  <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="/assets/css/styles.css" rel="stylesheet">
  @yield('style')
</head>
<body>
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/home">Log<span>book</span></a>
        <ul class="user-menu">
          <li class="dropdown pull-right">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> {{{ (Auth::user()->name) }}}  {{{ (Auth::user()->lastname) }}} <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
<<<<<<< HEAD
              <li>
                  <a href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                      Logout
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
              </li>
=======
              <li><a href="{{URL::route('logout')}}"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Cerrar sesión</a></li>
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
              <li><a href="/usuarios/{{{ (Auth::user()->id)}}}/edit"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"/></svg>Editar Perfil</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div><!-- /.container-fluid -->
  </nav>
  <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <ul class="nav menu">
      <li class="divider"></li>
      <li class="parent ">
        <a href="#">
        <span data-toggle="collapse" href="#sub-item-3"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"/></svg> Calendario</span>
        </a>
        <ul class="children collapse" id="sub-item-3">
          <div class="panel panel-blue">
            <div class="panel-body">
              <div id="calendar"></div>
            </div>
          </div>
        </ul>
      </li>
      <li><a href="/tareas"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Tareas</a></li>
      <li class="parent ">
        <a href="#">
          <span data-toggle="collapse" href="#sub-item-4"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg>Reportes</span>
        </a>
        <ul class="children collapse" id="sub-item-4">
          <li class="parent ">
            <a href="#">
              <span data-toggle="collapse" href="#sub-item-5"><svg class="glyph stroked chevron-down"><use xlink:href="#stroked-chevron-right"></use></svg> Empleados</span>
            </a>
            <ul class="children collapse" id="sub-item-5">
              <li class="ta">
                <a class="" href="/reporte_empleados">
                  <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Reporte Semanal
                </a>
              </li>
              <li class="ta">
                <a class="" href="/reporte_empleados_lapse">
                  <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Por periodo
                </a>
              </li>
            </ul>
          </li>
          <li>
            <a class="" href="/reporte_tarea">
              <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Tareas
            </a>
          </li>
          <li>
            <a class="" href="/reporte_insumos">
              <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Insumos
            </a>
          </li>
        </ul>
      </li>
      <li class="parent ">
        <a href="#">
          <span data-toggle="collapse" href="#sub-item-1"><svg class="glyph stroked chevron-down"><use xlink:href="#stroked-chevron-down"></use></svg></span> Empleados
        </a>
        <ul class="children collapse" id="sub-item-1">
          <li>
            <a class="" href="/empleados">
              <svg class="glyph stroked male user "><use xlink:href="#stroked-male-user"/></svg>
 Ver Empleados
            </a>
          </li>
          <li>
            <a class="" href="/categoria_empleado">
              <svg class="glyph stroked eye"><use xlink:href="#stroked-eye"/></svg> Ver Categorias
            </a>
          </li>
        </ul>
      </li>
<<<<<<< HEAD
        <li><a href="/unidad"><svg class="glyph stroked tag"><use xlink:href="#stroked-tag"/></svg> Unidades de medida</a></li>
=======
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
      <li class="parent ">
        <a href="#">
          <span data-toggle="collapse" href="#sub-item-2"><svg class="glyph stroked chevron-down"><use xlink:href="#stroked-chevron-down"></use></svg></span> Insumos
        </a>
        <ul class="children collapse" id="sub-item-2">
          <li>
            <a class="" href="/insumos">
              <svg class="glyph stroked eye"><use xlink:href="#stroked-eye"/></svg> Ver Insumos
            </a>
          </li>
          <li>
            <a class="" href="/categoria_insumos">
             <svg class="glyph stroked eye"><use xlink:href="#stroked-eye"/></svg> Ver Categorias
            </a>
          </li>
        </ul>
      </li>
      <li><a href="/labores"><svg class="glyph stroked sound on"><use xlink:href="#stroked-sound-on"/></svg>
 Labores</a></li>
      <li><a href="/lote"><svg class="glyph stroked location pin"><use xlink:href="#stroked-location-pin"/></svg> Lotes</a></li>
      <li><a href="/productos"><svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg> Productos</a></li>
<<<<<<< HEAD
      <?php $perfil =Auth::user()->isAdmin;?>
         @if($perfil==1)
      <li><a href="/usuarios"><svg class="glyph stroked male user "><use xlink:href="#stroked-male-user"/></svg>
          Usuarios</a></li>
=======
      <?php $perfil =Auth::user()->esAdmin;?>
         @if($perfil==1)
      <li><a href="/usuarios"><svg class="glyph stroked male user "><use xlink:href="#stroked-male-user"/></svg>
 Usuarios</a></li>
>>>>>>> f067d63699a17b92d6acba30915213898a64db5c
        @endif
    </ul>
  </div><!--/.sidebar-->
  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
      <div class="col-lg-12 line">
        @yield('content')
      </div><!--/.row-->
    </div><!--/.row-->
  </div>  <!--/.main-->
  <script src="/assets/js/jquery-1.12.3.min.js"></script>
  <script src="/assets/js/bootstrap-datepicker.min.js"></script>
  <script src="/assets/js/bootstrap.min.js"></script>
  <script src="/assets/js/lumino.glyphs.js"></script>
  <script src="/assets/js/calendar.js"></script>
  @yield('table')
</body>
</html>
