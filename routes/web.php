<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('obtenerInsumos/{id}','TareaController@getSuppliesByid');

Route::get('tareas', 'TareaController@index')->name('tareas');
Route::get('tareas/create', 'TareaController@create')->name('tareas.create');
Route::post('tareas/create','TareaController@save')->name('tareas.create');
Route::post('tareas/{id}/edit','TareaController@update2')->name('tareas.edit');
Route::get('tareas/{id}/edit','TareaController@edit')->name('tareas.edit');
Route::delete('/tareas/{id}','TareaController@destroy');

Route::resource('empleados', 'EmpleadoController');
Route::resource('insumos', 'InsumoController');
Route::resource('productos', 'ProductoController');
Route::resource('labores', 'LaborController');
Route::resource('unidad', 'UnidadController');
Route::resource('categoria_empleado', 'Categoria_empleadoController');
Route::resource('categoria_insumos', 'Categoria_insumosController');
Route::resource('lote', 'LoteController');

//reporte empleado -semanal
Route::get('reporte_empleados','ReporteController@indexEmpleados');
Route::post('reporte_empleados','ReporteController@showEmpleados');

Route::resource('tareas/report','TareaController@report');
//reporte empleado -rango de fecha
Route::get('reporte_empleados_lapse','ReporteController@indexEmpleadosLapse');
Route::post('reporte_empleados_lapse','ReporteController@showEmpleadosLapse');

Route::get('reporte_insumos','ReporteController@indexInsumos');
Route::post('reporte_insumos','ReporteController@showInsumos');

//reporte de tareas
Route::get('reporte_tarea','ReporteController@indexTarea');
Route::post('reporte_tarea','ReporteController@showTareas');

Route::post('getLabors','ReporteController@showLabores');


Route::get('insumos/{id}/recharge','InsumoController@recharge');
Route::post('insumos/save_recharge/{id}','InsumoController@save_recharge');

Route::get('dropdown', function(){
	$id = Request::input('option');
	$insumos = Categoria_Insumo::find($id)->insumos;
	return $insumos->lists('id', 'descripcion');
});
Route::put('usuarios/changepass/','UsuarioController@updatePassword')->name('changepass');
Route::resource('usuarios','UsuarioController');

Route::get('getSupplies/{id}','TareaController@getDataTableSupplies');
Route::get('getEmployees/{id}','TareaController@getEmployees')->name('getEmployees');

//Para llenar tabla de tareas

Route::post('getInfo','ReporteController@create');

Route::post('obtenerReporte','TareaController@getInfo');



Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', 'HomeController@index')->name('home');

Route::post('login', 'Auth\LoginController@authenticate')->name('login');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@registerUser')->name('register');
