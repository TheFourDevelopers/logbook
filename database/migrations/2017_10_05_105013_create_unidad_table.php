<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('unidades', function(Blueprint $table) {
          $table->increments('id');
          $table->string('nombre');
          $table->string('siglas');
          $table->dateTime('deleted_at')->nullable();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('unidades');
    }
}
