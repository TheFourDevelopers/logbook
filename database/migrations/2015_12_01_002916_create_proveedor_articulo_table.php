<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProveedorArticuloTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
 public function up()
    {
        Schema::create('proveedor_articulo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('articulo_id')->unsigned();
            $table->integer('proveedor_id')->unsigned();

        });
        Schema::table('proveedor_articulo', function ($table) {
            $table->foreign('articulo_id')->references('id')->on('articulo');
            $table->foreign('proveedor_id')->references('id')->on('proveedor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('categoria_articulo', function ($table) {
        $table->dropForeign('proveedor_articulo_articulo_id_foreign');
        $table->dropForeign('proveedor_articulo_proveedor_id_foreign');
    });
       Schema::drop('categoria_articulo');
   }
}
