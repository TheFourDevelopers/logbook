<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTareaInsumoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('tarea_insumo', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('id_tarea')->unsigned();
          $table->foreign('id_tarea')
          ->references('id')->on('tarea')
          ->onDelete('cascade');

          $table->integer('id_insumo')->unsigned();
          $table->foreign('id_insumo')
          ->references('id')->on('insumo')
          ->onDelete('cascade');

          $table->double('cantidad_insumo')->default(0);
          $table->double('costo_insumo')->default(0);




      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('tarea_insumo');
    }
}
