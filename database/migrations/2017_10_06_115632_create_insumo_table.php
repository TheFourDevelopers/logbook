<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsumoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('insumo', function (Blueprint $table) {
          $table->increments('id');
          $table->string('descripcion')->unique();
          $table->string('unidad_medida');
          $table->double('precio_compra');
          $table->double('cantidad_disponible')->default(0);
          $table->integer('id_categoria_insumo')->unsigned();
          $table->foreign('id_categoria_insumo')
          ->references('id')->on('categoria_insumo')
          ->onDelete('cascade');
          $table->dateTime('deleted_at')->nullable();
          
          $table->integer('id_unidad')->unsigned();
          $table->foreign('id_unidad')
          ->references('id')->on('unidades')
          ->onDelete('cascade');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('insumo');
    }
}
