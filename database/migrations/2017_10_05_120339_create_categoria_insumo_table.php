<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriaInsumoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('categoria_insumo', function (Blueprint $table) {
          $table->increments('id');
          $table->string('descripcion')->unique();
          $table->timestamps();
          $table->dateTime('deleted_at')->nullable();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('categoria_insumo');
    }
}
