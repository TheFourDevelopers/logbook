<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriaArticuloTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
 public function up()
    {
        Schema::create('categoria_articulo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('articulo_id')->unsigned();
            $table->integer('categoria_id')->unsigned();

        });
        Schema::table('categoria_articulo', function ($table) {
            $table->foreign('articulo_id')->references('id')->on('articulo');
            $table->foreign('categoria_id')->references('id')->on('categoria');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     Schema::table('categoria_articulo', function ($table) {
        $table->dropForeign('categoria_articulo_articulo_id_foreign');
        $table->dropForeign('categoria_articulo_categoria_id_foreign');
    });
     Schema::drop('account');
 }
}
