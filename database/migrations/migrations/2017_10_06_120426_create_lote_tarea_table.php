<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoteTareaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('lote_tarea', function (Blueprint $table) {

          $table->integer('id_lote')->unsigned();
          $table->foreign('id_lote')
          ->references('id')->on('lote')
          ->onDelete('cascade');
          $table->integer('id_tarea')->unsigned();
          $table->foreign('id_tarea')
          ->references('id')->on('tarea')
          ->onDelete('cascade');
          $table->dateTime('deleted_at')->nullable();
          $table->integer('id')->nullable();
            $table->primary(array('id_lote', 'id_tarea'));

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('lote_tarea');
    }
}
