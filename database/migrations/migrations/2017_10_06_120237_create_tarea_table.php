<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTareaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('tarea', function (Blueprint $table) {
          $table->increments('id');
          $table->date('fecha');
          $table->double('costo_mano_obra')->default(0);
          $table->double('costo_insumos')->default(0);
          $table->integer('id_labor')->unsigned();
          $table->foreign('id_labor')
          ->references('id')->on('labor')
          ->onDelete('cascade');

          $table->integer('id_producto')->unsigned();
          $table->foreign('id_producto')
          ->references('id')->on('producto')
          ->onDelete('cascade');
          $table->string('obervaciones');
          $table->dateTime('deleted_at')->nullable();
          $table->timestamp('created_at');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('tarea');
    }
}
