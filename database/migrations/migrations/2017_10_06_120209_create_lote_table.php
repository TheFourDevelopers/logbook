<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('lote', function (Blueprint $table) {
          $table->increments('id');
          $table->string('descripcion');
          $table->string('ubicacion');
          $table->timestamps();
          $table->dateTime('deleted_at')->nullable();
          $table->integer('superficie')->nullable();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lote');
    }
}
