<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadoTareaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('empleado_tarea', function (Blueprint $table) {
        $table->integer('id_empleado')->unsigned();
        $table->foreign('id_empleado')
        ->references('id')->on('empleado')
        ->onDelete('cascade');

        $table->integer('id_tarea')->unsigned();
        $table->foreign('id_tarea')
        ->references('id')->on('tarea')
        ->onDelete('cascade');
          $table->double('costo_individual')->default(0);
          $table->dateTime('deleted_at')->nullable();
          $table->integer('id')->nullable();
          $table->double('horas')->default(0)->nullable();
          $table->double('unidades')->default(0)->nullable();
          $table->double('precio')->default(0)->nullable();
          $table->boolean('destajo')->nullable();
          $table->primary(array('id_empleado', 'id_tarea'));


      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empleado_tarea');
    }
}
