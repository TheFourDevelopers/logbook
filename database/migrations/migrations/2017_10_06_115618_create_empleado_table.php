<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('empleado', function (Blueprint $table) {
          $table->increments('id');
          $table->string('cedula')->unique();
          $table->string('nombre',20);
          $table->string('apellidos',40);
          $table->string('domicilio', 20);
          $table->integer('id_categoria_empleado')->unsigned();
          $table->foreign('id_categoria_empleado')
          ->references('id')->on('categoria_empleado')
          ->onDelete('cascade');
          $table->string('telefono_casa', 20);
          $table->string('telefono_personal', 20);
          $table->dateTime('deleted_at')->nullable();
          $table->double('precio')->nullable();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empleado');
    }
}
