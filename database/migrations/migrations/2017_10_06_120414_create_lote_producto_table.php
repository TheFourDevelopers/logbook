<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoteProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('lote_producto', function (Blueprint $table) {
          $table->increments('id');
          $table->date('fecha');
          $table->integer('id_lote')->unsigned();
          $table->foreign('id_lote')
          ->references('id')->on('lote')
          ->onDelete('cascade');
          $table->integer('id_producto')->unsigned();
          $table->foreign('id_producto')
          ->references('id')->on('producto')
          ->onDelete('cascade');
          
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('lote_producto');
    }
}
